import csv


def team_win_details(file_):
    try:
        team_win_status = {}
        with open(file_) as matches_information:
            years = ["2015", "2016", "2017"]
            match_details = csv.DictReader(matches_information)
            for match in match_details:
                season = match['season']
                winner = match['winner']
                if winner in team_win_status:
                    if season in team_win_status[winner]:
                        team_win_status[winner][season] += 1
                    else:
                        team_win_status[winner][season] = 1
                else:
                    team_win_status[winner] = {season: 1}
                for year in years:
                    if year not in team_win_status[winner]:
                        team_win_status[winner][year] = 0

        return team_win_status

    except FileNotFoundError:
        return "File not found!!"
    except TypeError:
        return "Wrong Type!!"
    except OSError:
        return "Bad File Input!!"
    except KeyError:
        return "Mismatched Key found or searching in a wrong file !!"
    except NameError:
        return "Tried to use a variable or function name!!"


team_win_details('mock_matches.csv')
