import unittest
from economical_bowler import *


class EconomyTest(unittest.TestCase):

    def test_exceptions(self):
        expected_output = "File not found!!"
        input_file = "mock_deliverie.csv"
        input_year = "2016"
        output = economy_details(input_file, input_year)
        self.assertEqual(output, expected_output)

        expected_output = "Wrong Type!!"
        input_file = {}
        input_year = "2016"
        output = economy_details(input_file, input_year)
        self.assertEqual(output, expected_output)

        expected_output = "Bad File Input!!"
        input_file = 10
        input_year = "2016"
        output = economy_details(input_file, input_year)
        self.assertEqual(output, expected_output)

        expected_output = "Mismatched Key found or searching in a wrong file !!"
        input_file = "mock_matches.csv"
        input_year = "2016"
        output = economy_details(input_file, input_year)
        self.assertEqual(output, expected_output)

    def test_incorrect_output(self):

        input_file = "mock_deliveries.csv"
        input_year = "2020"
        expected_output = {}
        output = economy_details(input_file, input_year)
        self.assertEqual(output, expected_output)

        input_file = "mock_deliveries.csv"
        input_year = "2019"
        expected_output = {}
        output = economy_details(input_file, input_year)
        self.assertEqual(output, expected_output)

        input_file = "mock_deliveries.csv"
        input_year = "20989"
        expected_output = {}
        output = economy_details(input_file, input_year)
        self.assertEqual(output, expected_output)

    def test_economy_details_correct_output(self):

        input_file = "mock_deliveries.csv"
        input_year = "2016"
        output = economy_details(input_file, input_year)
        expected_output = {'RP Singh': {'runs': 8, 'balls': 6}, 'UT Yadav': {
            'runs': 10, 'balls': 6, }, 'JW Hastings': {'runs': 0, 'balls': 6}}
        self.assertEqual(output, expected_output)

        input_file = "mock_deliveries.csv"
        input_year = "2015"
        output = economy_details(input_file, input_year)
        expected_output = {'UT Yadav': {'runs': 3, 'balls': 6}, 'M Morkel': {
            'runs': 5, 'balls': 6}, 'JA Morkel': {'runs': 4, 'balls': 3}}
        self.assertEqual(output, expected_output)

        input_file = "mock_deliveries.csv"
        input_year = "2017"
        output = economy_details(input_file, input_year)
        expected_output = {'TS Mills': {'runs': 6, 'balls': 6}}
        self.assertEqual(output, expected_output)

    def test_economy_rates_calculation_corrected_output(self):

        input_file = "mock_deliveries.csv"
        input_year = "2017"
        output = economy_rate_calculation(input_file, input_year)
        expected_output = {'TS Mills': {'runs': 6, 'balls': 6, 'economy': 6.0}}
        self.assertEqual(output, expected_output)

        input_file = "mock_deliveries.csv"
        input_year = "2016"
        output = economy_rate_calculation(input_file, input_year)
        expected_output = {'RP Singh': {'runs': 8, 'balls': 6, 'economy': 8.0}, 'UT Yadav': {
            'runs': 10, 'balls': 6, 'economy': 10.0}, 'JW Hastings': {'runs': 0, 'balls': 6, 'economy': 0.0}}
        self.assertEqual(output, expected_output)

        input_file = "mock_deliveries.csv"
        input_year = "2015"
        output = economy_rate_calculation(input_file, input_year)
        expected_output = {'UT Yadav': {'runs': 3, 'balls': 6, 'economy': 3.0}, 'M Morkel': {
            'runs': 5, 'balls': 6, 'economy': 5.0}, 'JA Morkel': {'runs': 4, 'balls': 3, 'economy': 8.0}}
        self.assertEqual(output, expected_output)
