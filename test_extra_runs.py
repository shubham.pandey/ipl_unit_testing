import unittest
from extra_runs_ import *
# from common import *


class Test_Extra_Runs(unittest.TestCase):

    def test_exceptions(self):
        expected_output = "File not found!!"
        input_file = "mock_deliverie.csv"
        input_year = "2016"
        output = extra_runs_per_team(input_file, input_year)
        self.assertEqual(output, expected_output)

        expected_output = "Wrong Type!!"
        input_file = {}
        input_year = "2016"
        output = extra_runs_per_team(input_file, input_year)
        self.assertEqual(output, expected_output)

        expected_output = "Bad File Input!!"
        input_file = 10
        input_year = "2016"
        output = extra_runs_per_team(input_file, input_year)
        self.assertEqual(output, expected_output)

        expected_output = "Mismatched Key found or searching in a wrong file !!"
        input_file = "mock_matches.csv"
        input_year = "2016"
        output = extra_runs_per_team(input_file, input_year)
        self.assertEqual(output, expected_output)

    def test_incorrect_output(self):

        input_file = "mock_deliveries.csv"
        input_year = "2020"
        expected_output = {}
        output = extra_runs_per_team(input_file, input_year)
        self.assertNotEqual(output, expected_output)

        input_file = "mock_deliveries.csv"
        input_year = "2019"
        expected_output = {}
        output = extra_runs_per_team(input_file, input_year)
        self.assertNotEqual(output, expected_output)

        input_file = "mock_deliveries.csv"
        input_year = "20989"
        expected_output = {}
        output = extra_runs_per_team(input_file, input_year)
        self.assertNotEqual(output, expected_output)

    def test_correct_output(self):

        input_file = "mock_deliveries.csv"
        input_year = "2015"
        output = extra_runs_per_team(input_file, input_year)
        expected_output = {'Kolkata Knight Riders': 1, 'Delhi Daredevils': 4}
        self.assertEqual(output, expected_output)
