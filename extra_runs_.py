import csv
from common import *
matches_file = 'mock_matches.csv'
deliveries_file = 'mock_deliveries.csv'
year_to_calculate = "2015"


match_id_list = read_matches(matches_file, year_to_calculate)


def extra_runs_per_team(deliveries_file, year_to_calculate):
    try:
        with open(deliveries_file) as delivery_details_for_year:
            teams = {}
            delivery_data = csv.DictReader(delivery_details_for_year)
            for deliveries in delivery_data:
                if deliveries['match_id'] in match_id_list:
                    bowling_team = deliveries['bowling_team']
                    extra_runs = int(deliveries['extra_runs'])
                    if bowling_team not in teams:
                        teams[bowling_team] = 0
                    else:
                        teams[bowling_team] += extra_runs
            return teams

    except FileNotFoundError:
        return "File not found!!"
    except TypeError:
        return "Wrong Type!!"
    except OSError:
        return "Bad File Input!!"
    except KeyError:
        return "Mismatched Key found or searching in a wrong file !!"
    except NameError:
        return "Tried to use a variable or function name!!"


print(extra_runs_per_team(deliveries_file, year_to_calculate))
