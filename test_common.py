import unittest
from common import *


class TestCommonDependency(unittest.TestCase):

    def test_exceptions(self):

        input_file = "matches.csv"
        input_year = "2015"
        output = read_matches(input_file, input_year)
        expected_output = "File not found!!"
        self.assertEqual(output, expected_output)

        input_file = ""
        input_year = "2015"
        output = read_matches(input_file, input_year)
        expected_output = "File not found!!"
        self.assertEqual(output, expected_output)

        input_file = 88
        input_year = "2015"
        output = read_matches(input_file, input_year)
        expected_output = "Bad File descripter or wrong input for file!!"
        self.assertEqual(output, expected_output)

        input_file = []
        input_year = "2015"
        output = read_matches(input_file, input_year)
        expected_output = "Wrong type used somewhere!!"
        self.assertEqual(output, expected_output)

        input_file = "mock_deliveries.csv"
        input_year = "2015"
        output = read_matches(input_file, input_year)
        expected_output = "Wrong key/searching in  wrong file"
        self.assertEqual(output, expected_output)

    def test_wrong_output(self):

        input_file = "mock_matches.csv"
        input_year = "2020"
        expected_output = []
        output = read_matches(input_file, input_year)
        self.assertEqual(output, expected_output)

        input_file = "mock_matches.csv"
        input_year = "2019"
        expected_output = []
        output = read_matches(input_file, input_year)
        self.assertEqual(output, expected_output)

        input_file = "mock_matches.csv"
        input_year = "20989"
        expected_output = []
        output = read_matches(input_file, input_year)
        self.assertEqual(output, expected_output)

    def test_correct_output(self):

        input_file = "mock_matches.csv"
        input_year = "2015"
        expected_output = ['518', '519']
        output = read_matches(input_file, input_year)
        self.assertEqual(output, expected_output)

        input_file = "mock_matches.csv"
        input_year = "2016"
        expected_output = ['577', '578']
        output = read_matches(input_file, input_year)
        self.assertEqual(output, expected_output)

        input_file = "mock_matches.csv"
        input_year = "2017"
        expected_output = ['1', '2']
        output = read_matches(input_file, input_year)
        self.assertEqual(output, expected_output)
