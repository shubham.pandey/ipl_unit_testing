import csv


def mock_data_reader(file_):
    try:
        match_per_season_count = {}
        with open(file_) as matches_file:
            match_details = csv.DictReader(matches_file)
            for lines in match_details:
                season = lines['season']
                if season not in match_per_season_count:
                    match_per_season_count[season] = 0
                match_per_season_count[season] += 1
        return match_per_season_count

    except FileNotFoundError:
        return "File not found!!"
    except TypeError:
        return "Wrong Type!!"
    except OSError:
        return "Bad File Input!!"
    except KeyError:
        return "Mismatched Key found or searching in a wrong file !!"
    except NameError:
        return "Tried to use a variable or function name!!"


def execute(file_):
    mock_data_reader(file_)


execute('mock_matches.csv')
