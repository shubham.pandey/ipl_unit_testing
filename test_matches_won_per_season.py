import unittest
from matches_won_per_season import *


class Test_Matches_Won_By_Teams(unittest.TestCase):

    def test_exceptions(self):

        expected_output = "File not found!!"
        input_file = "mock_match.csv"
        output = team_win_details(input_file)
        self.assertEqual(output, expected_output)

        expected_output = "Wrong Type!!"
        input_file = {}
        output = team_win_details(input_file)
        self.assertEqual(output, expected_output)

        expected_output = "Bad File Input!!"
        input_file = 10
        output = team_win_details(input_file)
        self.assertEqual(output, expected_output)

        expected_output = "Mismatched Key found or searching in a wrong file !!"
        input_file = "mock_deliveries.csv"
        output = team_win_details(input_file)
        self.assertEqual(output, expected_output)

    def test_correct_output(self):
        input_file = 'mock_matches.csv'
        expected_output = {'Sunrisers Hyderabad': {'2017': 1, '2015': 0, '2016': 0}, 'Rising Pune Supergiant': {'2017': 1, '2015': 0, '2016': 0}, 'Kolkata Knight Riders': {
            '2015': 1, '2016': 1, '2017': 0}, 'Chennai Super Kings': {'2015': 1, '2016': 0, '2017': 0}, 'Rising Pune Supergiants': {'2016': 1, '2015': 0, '2017': 0}}
        output = team_win_details(input_file)
        self.assertEqual(output, expected_output)

    def test_incorrect_output(self):

        input_file = 'mock_matches.csv'
        expected_output = {}
        output = team_win_details(input_file)
        self.assertNotEqual(output, expected_output)

        input_file = 'mock_matches.csv'
        expected_output = []
        output = team_win_details(input_file)
        self.assertNotEqual(output, expected_output)
