import unittest
from matches_per_season import *


class Test_Matches_Played(unittest.TestCase):

    def test_exceptions(self):

        expected_output = "File not found!!"
        input_file = "mock_match.csv"
        output = mock_data_reader(input_file)
        self.assertEqual(output, expected_output)

        expected_output = "Wrong Type!!"
        input_file = {}
        output = mock_data_reader(input_file)
        self.assertEqual(output, expected_output)

        expected_output = "Bad File Input!!"
        input_file = 10
        output = mock_data_reader(input_file)
        self.assertEqual(output, expected_output)

        expected_output = "Mismatched Key found or searching in a wrong file !!"
        input_file = "mock_deliveries.csv"
        output = mock_data_reader(input_file)
        self.assertEqual(output, expected_output)

    def test_correct_output(self):
        expected_output = {"2017": 2, "2016": 2, "2015": 2}
        output = mock_data_reader('mock_matches.csv')
        self.assertEqual(output, expected_output)
