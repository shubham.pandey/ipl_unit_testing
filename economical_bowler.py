import csv
from common import *


matches_file = 'mock_matches.csv'
deliveries_file = 'mock_deliveries.csv'


def economy_details(name, year_to_match):
    id_list = read_matches(matches_file, year_to_match)
    bowlers = {}
    try:
        with open(name) as deliveries_file:
            deliveries_data = csv.DictReader(deliveries_file)
            for deliveries in deliveries_data:
                bowler_name = deliveries['bowler']
                runs_to_add = int(
                    deliveries['total_runs'])-(int(deliveries['legbye_runs'])+int(deliveries['bye_runs']))
                if deliveries['match_id'] in id_list:
                    if bowler_name not in bowlers:
                        bowlers[bowler_name] = {"runs": 0, "balls": 0}
                    bowlers[bowler_name]["runs"] += int(runs_to_add)
                    if deliveries['noball_runs'] == "0" and deliveries['wide_runs'] == "0":
                        bowlers[bowler_name]['balls'] += 1
            return bowlers

    except FileNotFoundError:
        return "File not found!!"
    except TypeError:
        return "Wrong Type!!"
    except OSError:
        return "Bad File Input!!"
    except KeyError:
        return "Mismatched Key found or searching in a wrong file !!"
    except NameError:
        return "Tried to use a variable or function name!!"


def economy_rate_calculation(file_deliveries_data, year):
    bowler_details = economy_details(file_deliveries_data, year)
    for bowler_, data in sorted(bowler_details.items()):
        overs = bowler_details[bowler_]['balls']/6
        bowler_details[bowler_]['economy'] = bowler_details[bowler_]['runs']/overs
    return bowler_details


economy_rate_calculation(deliveries_file, "2015")
