IPL DATA PROJECT UNIT TESTING:

This project aims to build test cases for IPL data project

Tasks Given:
Write unit tests for all the functions of respective files and run those test cases on the respective functions.

Approach Followed:
1. Started with the unit test library of python.
2. Created seperate test-files for each file.
3. Write test functions for specific functions.
4. Implement edge cases like file not found exception , Type error and various other cases.