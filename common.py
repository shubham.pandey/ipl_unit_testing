import csv


def read_matches(name, year_to_calculate):
    try:
        with open(name) as matches_file:
            list_match_id = []
            matches_data = csv.DictReader(matches_file)
            for years in matches_data:
                season = years['season']
                id_season = years['id']
                if season == year_to_calculate:
                    list_match_id.append(id_season)
        return list_match_id
    except FileNotFoundError:
        return "File not found!!"
    except OSError:
        return "Bad File descripter or wrong input for file!!"
    except TypeError:
        return "Wrong type used somewhere!!"
    except KeyError:
        return "Wrong key/searching in  wrong file"


# print(read_matches("mock_matches.csv", "2015"))
